<?php
/*
Plugin Name:  Whatsapp Full With Google Maps
Plugin URI:   https://www.ideoviral.com.co/
Description:  Plugin whatsapp full with google maps business
Version:      1.0
Author:       David Fernando Valenzuela Pardo
Author URI:   https://www.behance.net/vlzdavid127aa9
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  whatsapp-map-full
*/

if ( ! defined( 'ABSPATH' ) ) exit;

class WhatsappIdeoViral{

    public function require_file(){
        require_once plugin_dir_path(__FILE__) . './style_whatsapp.php';
        require_once plugin_dir_path(__FILE__) . './codestar-framework/codestar-framework.php';
        require_once plugin_dir_path(__FILE__) . './admin.php';
        require_once plugin_dir_path(__FILE__) . './content/shortcode_whatsapp.php';
        require_once plugin_dir_path(__FILE__) . './content/shortcode_map.php';
    }



}
$app = new WhatsappIdeoViral();
$app->require_file();

