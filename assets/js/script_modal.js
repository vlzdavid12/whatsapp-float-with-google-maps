
document.getElementById('selectCity').value='';
document.getElementById('text-alert-whatsapp-chat').innerHTML = '';

let btn_whatsapp = document.querySelector('.btn-whatsapp-float');

btn_whatsapp.addEventListener('click', ()=>{
   let container = document.querySelector('.whatsapp-container');
   container.classList.add("block-whatsapp")
})


let closebtn = document.querySelector('.close-whatsapp');

closebtn.addEventListener('click', ()=>{
    let container = document.querySelector('.whatsapp-container');
    container.classList.remove("block-whatsapp");
    document.getElementById('text-alert-whatsapp-chat').innerHTML = '';
    let addressWhatsapp   = document.querySelector('.list-address-whatsapp');
    if(addressWhatsapp.classList.contains( 'content-block' )) {
        let selectCityView = document.querySelector('.select-city-view');
        addressWhatsapp.classList.remove('content-block');
        selectCityView.classList.remove('content-disable');
    }
})

let search = document.querySelector('#searchWhatsapp');

search.addEventListener('click', ()=>{
    let inputSelect = document.querySelector('#selectCity');
    if(inputSelect.value.length <= 0){
       document.getElementById('text-alert-whatsapp-chat').innerHTML = "<div class='error-service-chat'>Error! ingresa el puesto de servicio</div>"
    }else{
     let addressWhatsapp   = document.querySelector('.list-address-whatsapp');
     let selectCityView = document.querySelector('.select-city-view');
         addressWhatsapp.classList.add('content-block');
         selectCityView.classList.add('content-disable');

     let cloud  = document.querySelectorAll('.cloud-chat');
     cloud.forEach(item=>{
       if(item.getAttribute("city")){
           let city = item.getAttribute("city");
           if(inputSelect.value !== city){
               document.querySelector('.city-' + city).style.display='none';
           }else{
               document.querySelector('.city-' + city).style.display='block';
           }
       }
    })

    }
})

let backChat = document.getElementById('back-chat');

backChat.addEventListener('click', ()=>{
    let addressWhatsapp   = document.querySelector('.list-address-whatsapp');
    if(addressWhatsapp.classList.contains( 'content-block' )) {
        let selectCityView = document.querySelector('.select-city-view');
        addressWhatsapp.classList.remove('content-block');
        selectCityView.classList.remove('content-disable');
        document.getElementById('selectCity').value='';
        document.getElementById('text-alert-whatsapp-chat').innerHTML = '';
    }
})


