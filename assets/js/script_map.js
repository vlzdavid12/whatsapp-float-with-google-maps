// The following example creates complex markers to indicate beaches near
// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
// to the base of the flagpole.


const {lat_general, lng_general, markers, ico_point, size_icon, zoom_map } = frontend_object_whatsapp.data;

function initMap() {
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: zoom_map | 8,
        center: { lat: parseFloat(lat_general), lng: parseFloat(lng_general) },
    });
    setMarkers(map);
}

let icon;

if(ico_point.length > 0){
    icon = ico_point;
}else{
    icon = 'http://localhost:8888/prueba/wp-content/plugins/whatsapp-map-full/assets/images/ico-person.png'
}

let newArray = [];

markers.forEach((item, index) =>{
    let map = [  item.title_marker, parseFloat(item.coordinate.lat), parseFloat(item.coordinate.lng), index, item];
    newArray.push(map)
});

const beaches = newArray;


function setMarkers(map) {
    let size = size_icon.split(',');
    // Adds markers to the map.
    // Marker sizes are expressed as a Size of X,Y where the origin of the image
    // (0,0) is located in the top left of the image.
    // Origins, anchor positions and coordinates of the marker increase in the X
    // direction to the right and in the Y direction down.
    const image = {
        url: icon,
        // This marker is 20 pixels wide by 32 pixels high.
        size: new google.maps.Size(parseInt(size[0]), parseInt(size[1])),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(parseInt(size[0]), parseInt(size[1])),
    };


    const infowindow = new google.maps.InfoWindow({
        maxWidth:330,
    })

    google.maps.event.addListener(map, 'click', () => {
        infowindow.close();
    });

    for (let i = 0; i < beaches.length; i++) {
        const beach = beaches[i];
        const marker = new google.maps.Marker({
            position: { lat: beach[1], lng: beach[2] },
            map,
            icon: image,
            title: beach[0],
            zIndex: beach[3],
        });

        let  htmlBtn;

        if(beach[4].phone) {
            htmlBtn = `<a href="https://api.whatsapp.com/send?phone=${beach[4].phone}&text=Enque%20te%20podemos%20ayudar" target="_blanck" class="btn-whatsapp"> <i class="icon-btn-whatsapp"></i><span>Llamar</span></a>`;
        }else{
            htmlBtn = '';
        }
        const html = `<div class="container-info-map">
                    <h1 id="firstHeading" class="title">${beach[4].title_marker}</h1>
                    <div id="bodyContent">
                    <div class="conte-info-map">
                         <p class="p-info"><strong>Dirección: </strong> ${beach[4].address_city}</p>
                         <p class="p-info"><strong>Telefóno: </strong> ${beach[4].phone}</p>
                    </div> 
                            ${htmlBtn}
                        <a href="mailto:${beach[4].email}" style="text-align: center;  display: flex; color:#404040; justify-content: center;
align-items: center; margin-top: 15px; z-index: 9999; position: relative">
                        <i class="email-icon"></i>
                        ${beach[4].email}
                        </a>
                </div>
</div>`;

        marker.addListener("click", ()=>{
            infowindow.setContent(html);
            infowindow.open(map, marker);
        })
    }
}
