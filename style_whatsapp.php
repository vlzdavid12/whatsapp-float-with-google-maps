<?php
class StyleWhatsapp{

    /**
     * StyleWhatsapp constructor.
     */
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this,'style_js_whatsapp']);
        add_filter('script_loader_tag', [$this, 'mg_add_async_defer_attributes'], 10, 2);
    }


    public function style_js_whatsapp(){
        $my_options = get_option( 'whatsapp-full' );
        wp_enqueue_style('whatsapp_full_map_css', plugins_url('./assets/css/style.css', __FILE__) );
        wp_enqueue_script('whatsapp_full_map_js', plugins_url('./assets/js/script_map.js', __FILE__), array(), 1.0, true );
        wp_localize_script( 'whatsapp_full_map_js', 'frontend_object_whatsapp',
            array(
                'data' => $my_options,
            )
        );

        wp_enqueue_script('whatsapp_full_modal_js', plugins_url('./assets/js/script_modal.js', __FILE__), array(), 1.0, true );

        wp_register_script( 'map-google-async', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBIAtCcmtZIK9wVYdQjPf_nYiNUD0JuOMo&callback=initMap&libraries=&v=weekly', [], false, true );
        wp_enqueue_script( 'map-google-async' );
    }

    /**
     * Esta función agrega los parámetros "async" y "defer" a recursos de Javascript.
     * Solo se debe agregar "async" o "defer" en cualquier parte del nombre del
     * recurso (atributo "handle" de la función wp_register_script).
     *
     * @param $tag
     * @param $handle
     *
     * @return mixed
     */

    public function mg_add_async_defer_attributes( $tag, $handle ) {

        // Busco el valor "async"
        if( strpos( $handle, "async" ) ):
            $tag = str_replace(' src', ' async="async" src', $tag);
        endif;

        // Busco el valor "defer"
        if( strpos( $handle, "defer" ) ):
            $tag = str_replace(' src', ' defer="defer" src', $tag);
        endif;

        return $tag;
    }


}

new StyleWhatsapp();