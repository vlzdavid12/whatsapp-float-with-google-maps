<?php

class ShortcodeMapWhatsapp{

    /**
     * ShortcodeMapWhatsapp constructor.
     */
    public function __construct()
    {
        add_shortcode( 'map_whatsapp', [$this, 'mapShortcode'] );
    }

    public function mapShortcode(){
           ?>
        <div id="map" style="height: 800px; width: 100%;"></div>
<?php
    }

}
new ShortcodeMapWhatsapp();