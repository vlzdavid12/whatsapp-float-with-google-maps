<?php
class ShortcodeWhatsapp{

    /**
     * ShortcodeWhatsapp constructor.
     */
    public function __construct()
    {
        add_action('wp_head', array($this, 'view_whatsapp_web'));
    }

    public function view_whatsapp_web(){
        $options = get_option( 'whatsapp-full' ); // unique id of the framework
        ?>
        <div class="whatsapp-eideoviral">
            <div class="whatsapp-container animated">
                <div class="whatsapp-header">
                    <div class="row-whatsapp">
                        <div class="col">
                            <img src="<?php echo  plugins_url( './assets/images/whatsapp-header-image.png', dirname(__FILE__) ) ?>" alt="whatsapp"/>
                        </div>
                        <div class="col flex-end">
                            <div class="close-whatsapp"><span>&#x2715;</span></div>
                        </div>
                    </div>
                </div>
                <div class="body-chat">
                    <!--Content No.1 -->
                    <div class="select-city-view" >
                        <div class="cloud-chat">
                            <div class="row-whatsapp">
                                <?php if(!empty($options["img_auth"])){?>
                                <div style="width: 75px; height: 75px; display: flex; justify-content: center; align-items: center;">
                                    <img src="<?php echo $options["img_auth"]; ?>" class="img-businness" alt="businness"/>
                                </div>
                                <?php } ?>
                                <div class="text-content">
                                    <?php echo $options['description_1'];?>
                                </div>
                            </div>
                        </div>
                        <p><?php echo __('Seleccione la ciudad que quieres contactar', 'whatsapp-map-full'); ?> </p>
                        <div id="text-alert-whatsapp-chat"></div>
                        <label>
                            <select class="select" id="selectCity">
                                <option value="" selected>Seleccione la ciudad</option>
                                <option value="1"> Armenia</option>
                                <option value="2"> Barranquilla</option>
                                <option value="3"> Buenaventura</option>
                                <option value="4"> Bogotá </option>
                                <option value="5"> Bucaramanga </option>
                                <option value="6"> Cartagena </option>
                                <option value="7"> Cali </option>
                                <option value="8"> Manizales </option>
                                <option value="9"> Medellin </option>
                                <option value="10"> Pasto </option>
                                <option value="11"> Popayán </option>
                                <option value="12"> Pereira </option>
                                <option value="13"> Rioacha </option>
                                <option value="14"> Santa Marta </option>
                                <option value="15"> Valledupar </option>
                                <option value="16"> Yopal </option>
                            </select>
                            <i class="arrow"></i>
                        </label>
                        <button id="searchWhatsapp" class="btn-search">Buscar</button>
                    </div>
                    <!--End Content No.1 -->
                    <!--Content No.2 -->
                    <div class="list-address-whatsapp" >
                        <p> <?php echo $options['description_2'];?> </p>

                        <?php foreach ( $options['markers'] as $item){?>
                            <!--CLOUD LIST START-->
                            <div class="cloud-chat city-<?php echo $item['select_city'] ; ?>" city="<?php echo $item['select_city'] ; ?>">
                                <div class="row-whatsapp">
                                    <div class="col-description">
                                        <h6 class="title" ><?php echo $item['title_marker'] ; ?></h6>
                                        <p class="address" ><?php echo $item['address_city'] ; ?></p>
                                    </div>
                                    <?php if(!empty($item['phone'])){ ?>
                                    <div class="col-btn-call">
                                        <a target="_blank" href="https://api.whatsapp.com/send?phone=<?php echo $item['phone'] ?>&text=Gola%20este%20es%20un%20an%20escritura" class="btn-call">Llamar</a>
                                    </div>
                                     <?php } ?>
                                </div>
                            </div>
                            <!--END CLOUD LIST START-->
                        <?php } ?>


                        <a href="#" id="back-chat" class="btn-back">Volver</a>
                    </div>
                    <!--End Content No.2 -->
                </div>
            </div>
            <div style="position: relative; top: -40px; right: -15px; z-index: 99999;">
                <a class=" pulse">
                    <i class="btn-whatsapp-float"></i>
                </a>
            </div>
        </div>
<?php
    }


}
new ShortcodeWhatsapp();