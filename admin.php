<?php


// Control core classes for avoid errors
if (class_exists('CSF')) {

    //
    // Set a unique slug-like ID
    $prefix = 'whatsapp-full';

    //
    // Create options
    CSF::createOptions($prefix, array(
        'framework_title' => __('Whatsapp Framework <small>by IdeoViral</small>', 'whatsapp-map-full'),

        'menu_title' => 'Whatsapp Config',
        'menu_slug' => 'whatsapp-config-full',
        'menu_icon' => 'dashicons-whatsapp',

        'show_search' => false,
        'show_reset_all' => false,
        'show_reset_section' => false,


        'show_in_customizer' => false,

        'sticky_header' => false,

        'footer_credit' => wp_sprintf(__(' Developer for <a href="%s" target="_blank">ideoViral</a>  ', 'whatsapp-map-full'), __('https://www.ideoviral.com.co/')),

        'nav' => 'normal',
        'theme' => 'white',
        'class' => '',
    ));

    //
    // Create a section
    CSF::createSection($prefix, array(
        'title' => __('Whatsapp Config', 'whatsapp-map-full'),
        'fields' => array(

            array(
                'id' => 'markers',
                'type' => 'group',
                'title' => __('Markers', 'whatsapp-map-full'),
                'fields' => array(
                    array(
                        'id' => 'title_marker',
                        'type' => 'text',
                        'title' => __('Title Marker', 'whatsapp-map-full'),
                    ),
                    array(
                        'id' => 'select_city',
                        'type' => 'select',
                        'title' => __('City', 'whatsapp-map-full'),
                        'placeholder' => __('Select City', 'whatsapp-map-full'),
                        'options' => array(
                            '1' => 'Armenia',
                            '2' => 'Barranquilla',
                            '3' => 'Buenaventura',
                            '4' => 'Bogotá',
                            '5' => 'Bucaramanga',
                            '6' => 'Cartagena',
                            '7' => 'Cali',
                            '8' => 'Manizales',
                            '9' => 'Medellin',
                            '10' => 'Pasto',
                            '11' => 'Popayán',
                            '12' => 'Pereira',
                            '13' => 'Riohacha',
                            '14' => 'Santa Marta',
                            '15' => 'Valledupar',
                            '16' => 'Yopal',
                        ),
                        'default' => '4'
                    ),

                    array(
                        'id' => 'address_city',
                        'type' => 'text',
                        'title' => __('Address City', 'whatsapp-map-full'),
                    ),
                    array(
                        'id' => 'phone',
                        'type' => 'text',
                        'title' => __('Phone', 'whatsapp-map-full'),
                        'desc' => __('Number phone example: 3214567890 action whatsapp.', 'whatsapp-map-full'),
                    ),

                    array(
                        'id' => 'email',
                        'type' => 'text',
                        'title' => __('Email', 'whatsapp-map-full'),
                    ),

                    array(
                        'id' => 'coordinate',
                        'type' => 'fieldset',
                        'title' => __('Coordinates', 'whatsapp-map-full'),
                        'desc' => wp_sprintf(__(' For more information to bring the lat and lng in <a href="%s" target="_blank">Google Maps</a>  ', 'whatsapp-map-full'), __('https://www.google.com/maps/?hl=es')),
                        'fields' => array(
                            array(
                                'id' => 'lat',
                                'type' => 'text',
                                'title' => __('Lat', 'whatsapp-map-full'),
                            ),
                            array(
                                'id' => 'lng',
                                'type' => 'text',
                                'title' => __('Lng', 'whatsapp-map-full'),
                            ),
                        ),
                    ),

                ),



            ),


        )
    ));

    //
    // Create a section
    CSF::createSection($prefix, array(
        'title' => 'Style Config',


        'fields' => array(

            array(
                'id' => 'ico_point',
                'type' => 'upload',
                'title' => __('Icon Pointer', 'whatsapp-map-full'),
                'desc' => __('Icon pointer map size 35px * 35px', 'whatsapp-map-full'),
            ),

            array(
                'id' => 'size_icon',
                'type' => 'text',
                'title' => __('Size Icon', 'whatsapp-map-full'),
                'default' => '35, 35',
            ),

            array(
                'id' => 'img_auth',
                'type' => 'upload',
                'title' => __('Image Author', 'whatsapp-map-full'),
            ),

            array(
                'id' => 'description_1',
                'type' => 'textarea',
                'title' => __('Description Welcome', 'whatsapp-map-full'),
                'default' => __('Hola! 🚀 En que te podemos ayudar. ', 'whatsapp-map-full'),
            ),

            array(
                'id' => 'description_2',
                'type' => 'textarea',
                'title' => __('Description Search', 'whatsapp-map-full'),
                'default' =>  __('Estos son los puntos de servicios disponibles.', 'whatsapp-map-full'),
            ),

            array(
                'id' => 'lat_general',
                'type' => 'text',
                'title' => __('Lat', 'whatsapp-map-full'),
                'default' => '4.8065033',
            ),

            array(
                'id' => 'lng_general',
                'type' => 'text',
                'title' => __('Lng', 'whatsapp-map-full'),
                'default' => '-74.44505',
            ),

            array(
                'id' => 'zoom_map',
                'type' => 'text',
                'title' => __('Zoom map', 'whatsapp-map-full'),
                'default' => '12',
            ),



        ),


    ));


}


